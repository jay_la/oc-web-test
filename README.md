# Otium Circle Web App #

The official repo for Otium Circle PWA using NUXT.JS and packaging system NPM.

* OC Web App is in SSR Mode (NUXT Universal). 
* More about SSRs here https://medium.com/@baphemot/whats-server-side-rendering-and-do-i-need-it-cb42dc059b38
* Version: 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How to get started locally ###

* Clone the repo (you may need Git on your system)
* Install NPM globally (may be needed on production servers)
* Install Node globally (needed on production server)
* cd into repo (website)
* `npm install` to install all dependencies.
* To launch Nuxt in development mode with hot reloading `nuxt` or `npm run dev`
* To deploy or build/start in production, you need to build `nuxt build` or `npm run build` and start `nuxt start` or `npm run start`

More about commands and deployoment here https://nuxtjs.org/guide/commands/

Other sources that may help in deployment of nuxt on docker
* https://jonathanmh.com/deploying-a-nuxt-js-app-with-docker/
* https://dev.to/vuevixens/dockerise-your-nuxt-ssr-app-like-a-boss-a-true-vue-vixens-story-4mm6

mmmm

### Note: OC Web App is SSR, it should be built using the deploy method above, this may also apply on production servers.
### All node packages are in package.json
### Nuxt Configurations are in nuxt.config.js
